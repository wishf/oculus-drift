﻿using UnityEngine;
using System.Collections;
using Leap;

public class TestLeapThing : MonoBehaviour {

	Controller controller = null;

	// Use this for initialization
	void Start () {

	}

	void Awake() {
		controller = new Controller ();
	}

	Hand GetHand() {
		Frame frame = controller.Frame();
		HandList hands = frame.Hands;
		
		Hand firstHand = hands [0];

		return firstHand;
	}
	
	// Update is called once per frame
	void Update () {
		Hand hand = GetHand ();

		float yaw = hand.Direction.Yaw; /* Do we need this? */
		float pitch = hand.Direction.Pitch;
		float roll = hand.PalmNormal.Roll;

		/* TODO: Use this somehow */
		print ("Pitch = " + pitch);
	}
}
