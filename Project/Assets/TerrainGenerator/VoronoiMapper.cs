﻿using Assets.TerrainGenerator.External;
using BenTools.Mathematics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.TerrainGenerator
{
    public class VoronoiMapper : MonoBehaviour
    {
        private VoronoiGraph graph;
        private GameObject terrain;

        public Vector3 spawnLocation = new Vector3(0, 0, 0);
        public float xScale, yScale, zScale;
        
        public float MinMountainRatio = 0.2f;
        public float MinSigma = 0f;
        public float MaxSigma = 50f;
        
        public float PerlinRatio = 0.1f;
        public int PerlinPasses = 2;
        public float xPerlinOffset, yPerlinOffset;

        public int mountainCount = 5;

        public Material material;
        public int points;
        public int relaxationPasses;
        public int seed;

        public float mountainsideThreshold;
        public float mudslopeThreshold;
        public float forestThreshold;
        public float beachThreshold;

        private List<Vector2> xzOfVertices = new List<Vector2>();

        List<float> xs = new List<float>();
        List<float> ys = new List<float>();
        List<float> sigmas = new List<float>();
        List<float> scales = new List<float>();


        public VoronoiMapper()
        {
        }

        public void Generate(int nPoints, int relaxationPasses, int seed)
        {
            System.Random rng = new System.Random(seed);
            List<Vector> points = new List<Vector>(nPoints);

            for (int i = 0; i < nPoints; i++)
            {
                points.Add(new Vector(((rng.NextDouble() * 2) - 1) * 100, ((rng.NextDouble() * 2) - 1) * 100));
            }

            Dictionary<Vector, HashSet<Vector>> edgeMapping = new Dictionary<Vector,HashSet<Vector>>();

            for (int i = 0; i < relaxationPasses + 1; i++)
            {
                graph = Fortune.ComputeVoronoiGraph(points);

                if(i != relaxationPasses) {

                    foreach (var edge in graph.Edges)
                    {
                        if (!edgeMapping.ContainsKey(edge.LeftData))
                        {
                            edgeMapping.Add(edge.LeftData, new HashSet<Vector>());
                        }

                        if(edge.IsPartlyInfinite || 
                            Math.Abs(edge.VVertexA[0]) > 100 ||
                            Math.Abs(edge.VVertexA[1]) > 100 ||
                            Math.Abs(edge.VVertexB[0]) > 100 ||
                            Math.Abs(edge.VVertexB[1]) > 100) {
                            edgeMapping[edge.LeftData] = null;
                        }
                        else if(edgeMapping[edge.LeftData] != null) {
                            edgeMapping[edge.LeftData].Add(edge.VVertexA);
                            edgeMapping[edge.LeftData].Add(edge.VVertexB);
                        }

                        if (!edgeMapping.ContainsKey(edge.RightData))
                        {
                            edgeMapping.Add(edge.RightData, new HashSet<Vector>());
                        }

                        if(edge.IsPartlyInfinite || 
                            Math.Abs(edge.VVertexA[0]) > 100 ||
                            Math.Abs(edge.VVertexA[1]) > 100 ||
                            Math.Abs(edge.VVertexB[0]) > 100 ||
                            Math.Abs(edge.VVertexB[1]) > 100) {
                            edgeMapping[edge.RightData] = null;
                        }
                        else if(edgeMapping[edge.RightData] != null) {
                            edgeMapping[edge.RightData].Add(edge.VVertexA);
                            edgeMapping[edge.RightData].Add(edge.VVertexB);
                        }

                    }
                    points.Clear();

                    foreach (var pointCorners in edgeMapping)
                    {
                        if (pointCorners.Value == null)
                        {
                            points.Add(pointCorners.Key);
                        }
                        else
                        {
                            Vector centroid = Vector.zero;
                            foreach (var point in pointCorners.Value)
                            {
                                centroid += point;
                            }

                            centroid.Multiply(1.0 / pointCorners.Value.Count);

                            points.Add(centroid);
                        }
                    }
                }

                edgeMapping.Clear();
            }

            HashSet<Vector> junkVertices = new HashSet<Vector>();
            foreach (var edge in graph.Edges)
            {
                if (edge.IsPartlyInfinite || 
                    Math.Abs(edge.VVertexA[0]) > 100 ||
                    Math.Abs(edge.VVertexA[1]) > 100 ||
                    Math.Abs(edge.VVertexB[0]) > 100 ||
                    Math.Abs(edge.VVertexB[1]) > 100)
                {
                    junkVertices.Add(edge.LeftData);
                    junkVertices.Add(edge.RightData);
                }
            }

            int j = 0;
            foreach (var gVert in points)
            {
                if (!junkVertices.Contains(gVert))
                {
                    xzOfVertices.Add(new Vector2((float)gVert[0], (float)gVert[1]));
                    j++;
                }
            }
        }

        public GameObject ToMesh(float xScale, float yScale, float zScale)
        {

            Vector3[] Vertices = new Vector3[xzOfVertices.Count];
            Vector2[] UVs = new Vector2[xzOfVertices.Count];
            Color[] Colours = new Color[xzOfVertices.Count];
            for (int ii1 = 0; ii1 < xzOfVertices.Count; ii1++)
            {
                float x = xzOfVertices[ii1].x, y = xzOfVertices[ii1].y;
                float height = GetHeight(x,y);
                Vertices[ii1] = new Vector3((x / 10) * xScale, height, (y / 10) * zScale);
                Colours[ii1] = Color.Lerp(
                    new Color(1f, 207f/255f, 100f/255f),
                    Color.Lerp(
                        new Color(0f, 120f/255f, 35f/255f),
                        Color.Lerp(
                            new Color(120f/255f, 76f/255f, 34f/255f),
                            Color.Lerp(
                                Color.grey,
                                Color.white,
                                height - mountainsideThreshold),
                            height - mudslopeThreshold),
                        height - forestThreshold),
                    height - beachThreshold);
                UVs[ii1] = new Vector2((x / 10), (y / 10));
            }

            GameObject OurNewMesh = new GameObject("Terrain");
            Mesh mesh = new Mesh();
            mesh.vertices = Vertices;
            mesh.uv = UVs;
            mesh.triangles = Triangulator.TriangulatePolygon(xzOfVertices.ToArray());
            mesh.RecalculateNormals();
            mesh.colors = Colours;
            MeshFilter mf = OurNewMesh.AddComponent<MeshFilter>();
            OurNewMesh.AddComponent<MeshRenderer>();
            OurNewMesh.GetComponent<MeshRenderer>().material = material;
            //OurNewMesh.GetComponent().castShadows = false;
            //OurNewMesh.GetComponent().receiveShadows = false;
            TangentSolver(mesh);
            mf.mesh = mesh;
            mesh.RecalculateBounds();

            OurNewMesh.AddComponent<MeshCollider>();
            return OurNewMesh;

        }

        public float GetHeight(float x, float y)
        {

            float result = 0;

            for (int i = 0; i < mountainCount; i++)
            {
                result += (SampleGaussian(x, y, i)*scales[i]);
            }

            result += SamplePerlin(x, y, PerlinPasses);

            return result;
        }

        private float SamplePerlin(float x, float y, int level)
        {
            float result = 0;

            for (int i = 0; i < level; i++)
            {
                result += (yScale * PerlinRatio) * (Mathf.PerlinNoise(x + xPerlinOffset, y + yPerlinOffset));
            }

            return result;
        }

        private float SampleGaussian(float x, float y, int index)
        {
            float d = (float)Math.Sqrt(Math.Pow(x - xs[index], 2) + Math.Pow(y - ys[index], 2));
            return Mathf.Exp(-d * d / (sigmas[index] * sigmas[index]));
        }

        void Start()
        {
            for (int i = 0; i < mountainCount; i++)
            {
                xs.Add(UnityEngine.Random.Range(-(xScale / 2), xScale / 2));
                ys.Add(UnityEngine.Random.Range(-(zScale / 2), zScale / 2));
                sigmas.Add(UnityEngine.Random.Range(MinSigma, MaxSigma));
                scales.Add(UnityEngine.Random.Range(yScale * MinMountainRatio, yScale));
            }
            this.Generate(points, relaxationPasses, seed);
            terrain = this.ToMesh(xScale, yScale, zScale);
            Instantiate(terrain, spawnLocation, Quaternion.identity);
        }

        void Update()
        {

        }

        private void TangentSolver(Mesh theMesh)
        {
            int vertexCount = theMesh.vertexCount;
            Vector3[] vertices = theMesh.vertices;
            Vector3[] normals = theMesh.normals;
            Vector2[] texcoords = theMesh.uv;
            int[] triangles = theMesh.triangles;
            int triangleCount = triangles.Length / 3;
            Vector4[] tangents = new Vector4[vertexCount];
            Vector3[] tan1 = new Vector3[vertexCount];
            Vector3[] tan2 = new Vector3[vertexCount];
            int tri = 0;
            for (int i = 0; i < (triangleCount); i++)
            {
                int i1 = triangles[tri];
                int i2 = triangles[tri + 1];
                int i3 = triangles[tri + 2];

                Vector3 v1 = vertices[i1];
                Vector3 v2 = vertices[i2];
                Vector3 v3 = vertices[i3];

                Vector2 w1 = texcoords[i1];
                Vector2 w2 = texcoords[i2];
                Vector2 w3 = texcoords[i3];

                float x1 = v2.x - v1.x;
                float x2 = v3.x - v1.x;
                float y1 = v2.y - v1.y;
                float y2 = v3.y - v1.y;
                float z1 = v2.z - v1.z;
                float z2 = v3.z - v1.z;

                float s1 = w2.x - w1.x;
                float s2 = w3.x - w1.x;
                float t1 = w2.y - w1.y;
                float t2 = w3.y - w1.y;

                float r = 1.0f / (s1 * t2 - s2 * t1);
                Vector3 sdir = new Vector3((t2 * x1 - t1 * x2) * r, (t2 * y1 - t1 * y2) * r, (t2 * z1 - t1 * z2) * r);
                Vector3 tdir = new Vector3((s1 * x2 - s2 * x1) * r, (s1 * y2 - s2 * y1) * r, (s1 * z2 - s2 * z1) * r);

                tan1[i1] += sdir;
                tan1[i2] += sdir;
                tan1[i3] += sdir;

                tan2[i1] += tdir;
                tan2[i2] += tdir;
                tan2[i3] += tdir;

                tri += 3;
            }

            for (int i = 0; i < (vertexCount); i++)
            {
                Vector3 n = normals[i];
                Vector3 t = tan1[i];

                // Gram-Schmidt orthogonalize
                Vector3.OrthoNormalize(ref n, ref t);

                tangents[i].x = t.x;
                tangents[i].y = t.y;
                tangents[i].z = t.z;

                // Calculate handedness
                tangents[i].w = (Vector3.Dot(Vector3.Cross(n, t), tan2[i]) < 0.0) ? -1.0f : 1.0f;
            }
            theMesh.tangents = tangents;
        }
    }
}
