﻿using UnityEngine;
using System.Collections;

public class test : MonoBehaviour {

	// Use this for initialization
    private int Samples = 14;
    private float Width = 20;
    private float Scale = 1;

	void Start () {
        Mesh mesh = this.GetComponent<MeshFilter>().mesh;
        mesh.Clear();
        float edge = Width / (Samples - 1);

        // Create the vertices and texture coords
        var vertices = new Vector3[Samples * Samples];
        var uvs = new Vector2[Samples * Samples];

        // Fill in the data
        for (int i = 0, p = 0; i < Samples; i++)
        {
            for (int j = 0; j < Samples; j++)
            {
                // Current vertex
                Vector3 center = new Vector3((i * edge) - Width/2f, 0f, (j * edge) - Width/2f);
                // Height of this vertex (from heightmap)
                float h = SampleHeight(center.x, center.z);
                center.y = h;
                vertices[p] = center;
                // UV coords in [0,1] space
                uvs[p++] = new Vector2(i / (Samples - 1f),
                                       j / (Samples - 1f));
            }
        }

        int quadCount = (Samples - 1) * (Samples - 1);
        var triangles = new int[3 * 2 * quadCount];

        for (int t = 0; t < quadCount; t++)
        {
            int i = t / (Samples - 1);
            int j = t % (Samples - 1);

            triangles[6 * t + 0] = CalculateIndex(i + 1, j);
            triangles[6 * t + 1] = CalculateIndex(i, j);
            triangles[6 * t + 2] = CalculateIndex(i, j + 1);
            triangles[6 * t + 3] = CalculateIndex(i + 1, j);
            triangles[6 * t + 4] = CalculateIndex(i, j + 1);
            triangles[6 * t + 5] = CalculateIndex(i + 1, j + 1);
        }
        mesh.vertices = vertices;
        mesh.uv = uvs;
        mesh.triangles = triangles;
        mesh.RecalculateNormals();

        
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public float SampleHeight(float x, float y)
    {
        float ret =  Scale*Mathf.PerlinNoise(x, y);
        return ret;
    }
    private int CalculateIndex(int i, int j)
    {
        return i * Samples + j;
    }
}

