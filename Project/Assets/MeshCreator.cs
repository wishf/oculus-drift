﻿using UnityEngine;
using System.Collections;

public class MeshCreator : MonoBehaviour {

	// Use this for initialization
    private int Samples = 2;
    float width = 4;

	void Start () {
        Mesh m = new Mesh();
        m.name = "Scripted_Plane_New_Mesh";

        float edge = width / (Samples - 1);

        // Create the vertices and texture coords
        var vertices = new Vector3[Samples * Samples];
        var uvs = new Vector2[vertices.Length];

        // Fill in the data
        for (int i = 0, p = 0; i < Samples; i++)
        {
            for (int j = 0; j < Samples; j++)
            {
                // Current vertex
                var center = new Vector3(i * edge, 0, j * edge);
                // Height of this vertex (from heightmap)
                float h = SampleHeight(center.x, center.z);
                center.y = h;
                vertices[p] = center;
                // UV coords in [0,1] space
                uvs[p++] = new Vector2(i / (Samples - 1f),
                                       j / (Samples - 1f));
            }
        }
        m.vertices = vertices;

        foreach (Vector3 vector in vertices)
        {
        Debug.Log(vector.ToString());
        }

        for (var i = 0; i < uvs.Length; i++)
        {
            uvs[i] = new Vector2(vertices[i].x, vertices[i].z);
        }
        m.uv = uvs;

        int quadCount = (Samples - 1) * (Samples - 1);
        var triangles = new int[3 * 2 * quadCount];

        for (int t = 0; t < quadCount; t++)
        {
            int i = t / (Samples - 1);
            int j = t % (Samples - 1);

            triangles[6 * t + 0] = CalculateIndex(i + 1, j);
            triangles[6 * t + 1] = CalculateIndex(i, j);
            triangles[6 * t + 2] = CalculateIndex(i, j + 1);
            triangles[6 * t + 3] = CalculateIndex(i + 1, j);
            triangles[6 * t + 4] = CalculateIndex(i, j + 1);
            triangles[6 * t + 5] = CalculateIndex(i + 1, j + 1);
        }
        m.triangles = triangles;
        m.RecalculateNormals();
        GameObject obj = new GameObject("New_Plane_Fom_Script", 
            new System.Type [] { typeof(MeshRenderer), typeof(MeshFilter), typeof(MeshCollider) });
        obj.GetComponent<MeshFilter>().mesh = m;

        
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public float SampleHeight(float x, float y)
    {
        return 5*Mathf.PerlinNoise(x, y);
    }
    private int CalculateIndex(int i, int j)
    {
        return i * Samples + j;
    }
}

